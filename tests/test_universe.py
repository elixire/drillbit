# drillbit: aioinflux wrapper
# Copyright 2020, elixi.re Team and the drillbit contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

pytestmark = pytest.mark.asyncio


async def test_universe():
    assert 1 == 1
