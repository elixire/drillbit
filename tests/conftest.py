# drillbit: aioinflux wrapper
# Copyright 2020, elixi.re Team and the drillbit contributors
# SPDX-License-Identifier: AGPL-3.0-only


import os
import sys

# import pytest

sys.path.append(os.getcwd())
# from trampoline import create_redis  # noqa: E402


# @pytest.yield_fixture(name="client")
# async def client_fixture():
#    client = await create_redis(address=os.environ["REDIS_ADDR"])
#    yield client
