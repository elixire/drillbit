# drillbit: aioinflux wrapper
# Copyright 2020, elixi.re Team and the drillbit contributors
# SPDX-License-Identifier: AGPL-3.0-only

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name="drillbit",
    version="0.1.0",
    description="aioinflux wrapper",
    url="https://elixi.re",
    author="Ave Ozkal, Luna Mendes",
    author_email="drillbit@elixi.re",
    python_requires=">=3.8",
    install_requires=["aioinflux==0.9.0"],
    packages=["drillbit"],
    package_dir={"": "."},
    package_data={"drillbit": ["py.typed"]},
)
